"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(controlDist, brevetDist, brevetStartTime):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    brevetTime = arrow.get(brevetStartTime, 'YYYY-MM-DD HH:mm')
        
    #Used a while loop to make sure the code exits if it gets a value.
    hourValue = -1
    while hourValue == -1:
        if controlDist < 200:
            hourValue = controlDist/34
            break
        elif controlDist <= 400:
            hourValue = (controlDist-200)/32 + 200/34
            break
        elif controlDist <= 600:
            hourValue = (controlDist-400)/30 + 200/32 + 200/34
            break
        elif controlDist <= 1000:
            hourValue = (controlDist-600)/28 + 200/30 + 200/32 + 200/34
            break
        elif controlDist <= 1300:
            hourValue = (controlDist-1000)/26 + 200/28 + 200/30 + 200/32 + 200/34
            break

    brevetTime = brevetTime.shift(hours=+hourValue)
    brevetTime = brevetTime.isoformat()
    brevetTime = brevetTime[:-6]
    return brevetTime


def close_time(controlDist, brevetDist, brevetStartTime):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """

    brevetTime = arrow.get(brevetStartTime, 'YYYY-MM-DD HH:mm')
    
    #Used a while loop to make sure the code exits if it gets a value.
    hourValue = -1
    while hourValue == -1:
        if controlDist == 0:
            hourValue = 1
            break

        #French variation
        elif controlDist <= 60:
            hourValue = controlDist/20 + 1
            break     
        else:
            if controlDist == 200:
                hourValue = 13.5
                break
            elif controlDist == 300:
                hourValue = 20
                break
            elif controlDist == 400:
                hourValue = 27
                break
            elif controlDist == 600:
                hourValue = 40
                break
            elif controlDist == 1000:
                hourValue = 75
                break
            else:
                if controlDist <= 600:
                    hourValue = controlDist/15
                    break
                elif controlDist <= 1000:
                    hourValue = (controlDist-600)/11.428 + 600/15
                    break
                elif controlDist <= 1300:
                    hourValue = (controlDist-1000)/13.333 + 600/15 + 1000/11.428
                    break
        
    brevetTime = brevetTime.shift(hours=+hourValue)
    brevetTime = brevetTime.isoformat()
    brevetTime = brevetTime[:-6]
    return brevetTime
