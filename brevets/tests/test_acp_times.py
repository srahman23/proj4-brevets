"""
Nose tests for acp_times.py. The checks are calculated by the time differences of the minutes. The seconds are truncated instead of rounded.
"""

startTime = '2017-01-01 00:00'
from acp_times import close_time
from acp_times import open_time

from arrow import arrow, get
from datetime import datetime


def test_zeroValue():
   """
   First control of the race. 
   Close time hour should be T01 as it is one hour after race begins 1. 
   Open time should be same as start time. 
   """
   closeTime = close_time(0, 200, startTime)
   openTime = open_time(0, 200, startTime)

   succCloseTime = str(datetime(2017,1,1,1))
   succCloseTime = succCloseTime.replace(' ', 'T')

   succOpenTime = str(datetime(2017,1,1))
   succOpenTime = succOpenTime.replace(' ', 'T')

   assert closeTime == succCloseTime
   assert openTime == succOpenTime


def test_brevetDistance_200():
   """
   Total distance of the race set at 200 with the last control point being at 200
   """
   closeTime = close_time(200, 200, startTime)
   openTime = open_time(200, 200, startTime)

   succCloseTime = str(datetime(2017,1,1,13,30))
   succCloseTime = succCloseTime.replace(' ', 'T')

   succOpenTime = str(datetime(2017,1,1,5,52))
   succOpenTime = succOpenTime.replace(' ', 'T')
   
   assert closeTime[:16] == succCloseTime[:16]
   assert openTime[:16] == succOpenTime[:16]

def test_brevetDistance_400():
   """
   Total distance of the race set at 400 with the last control point being at 400
   """
   closeTime = close_time(400, 400, startTime)
   openTime = open_time(400, 400, startTime)

   succCloseTime = str(datetime(2017,1,2,3,0))
   succCloseTime = succCloseTime.replace(' ', 'T')

   succOpenTime = str(datetime(2017,1,1,12,7))
   succOpenTime = succOpenTime.replace(' ', 'T')
   
   print(openTime[:16],succOpenTime)
   assert closeTime[:16] == succCloseTime[:16]
   assert openTime[:16] == succOpenTime[:16]

def test_brevetDistance_600():
   """
   Total distance of the race set at 600 with the last control point being at 600
   """
   closeTime = close_time(600, 600, startTime)
   openTime = open_time(600, 600, startTime)


   succCloseTime = str(datetime(2017,1,2,16,0))
   succCloseTime = succCloseTime.replace(' ', 'T')
   
   print(closeTime[:16],succCloseTime[:16])
   succOpenTime = str(datetime(2017,1,1,18,47))
   succOpenTime = succOpenTime.replace(' ', 'T')
   print(openTime[:16],succOpenTime[:16])
   assert closeTime[:16] == succCloseTime[:16]
   assert openTime[:16] == succOpenTime[:16]

def test_brevetDistance_1000():
   """
   Total distance of the race set at 1000 with the last control point being at 1000
   """
   closeTime = close_time(1000, 1000, startTime)
   openTime = open_time(1000, 1000, startTime)

   succCloseTime = str(datetime(2017,1,4,3,0))
   succCloseTime = succCloseTime.replace(' ', 'T')
  
   succOpenTime = str(datetime(2017,1,2,9,5))
   succOpenTime = succOpenTime.replace(' ', 'T')
   print(closeTime[:16],succCloseTime)
   assert closeTime[:16] == succCloseTime[:16]
   assert openTime[:16] == succOpenTime[:16]

def test_weird_input():
   closeTime = close_time(1000, 1000, "blah")
   openTime = open_time(1000, 1000, 42)




