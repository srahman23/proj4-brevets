# Project 4: Brevet time calculator with Ajax
Calculates the opening and closing times of a brevet control point based on distances given. Values are truncated instead of rounded so outputs might be off by 1 minute.

Uses the french variation for distances below 60. 
